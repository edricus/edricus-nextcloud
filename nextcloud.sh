#!/bin/bash -e
# Installer Nextcloud avec Redis
if [[ "$EUID" -ne 0 ]]; then 
  echo "Please run as root"
  exit
fi

questions() {
VERIFY=1
while [[ ${NEXTCLOUDPASSWD} != ${VERIFY} ]]; do
 read -rp " -> Name of the Nextcloud user: " NEXTCLOUDUSER
 read -rsp " -> Password for ${NEXTCLOUDUSER}: " NEXTCLOUDPASSWD
 printf "\n"
 read -rsp " -> verify password: " VERIFY
 printf "\n"
 if [[ ${NEXTCLOUDPASSWD} != ${VERIFY} ]]; then
     sleep 1
     echo "passwords doesn't match, retry"
 fi
done
read -rp 'Name of the server (default: nextcloud) : ' SERVERNAME
if [[ -z ${SERVERNAME} ]]; then
  SERVERNAME=nextcloud
fi
}

dependencies_install() {
# redis available on php 7.4 only
apt-get install -yqq php7.4-{curl,gd,mbstring,xml,zip,mysql,bz2,intl} libapache2-mod-php7.4 php7.4 apache2 mariadb-server php-redis redis
}

mariadb_configuration() {
mysql_secure_installation
mysql << MYSQL
create user "${NEXTCLOUDUSER}"@'localhost' identified by "${NEXTCLOUDPASSWD}";
create database nextcloud;
GRANT ALL PRIVILEGES ON nextcloud.* TO "${NEXTCLOUDUSER}"@'localhost';
FLUSH PRIVILEGES;
MYSQL
}

nextcloud_download() {
wget https://download.nextcloud.com/server/releases/nextcloud-23.0.0.zip -P "${NEXTCLOUD_LOCATION}"/
unzip "${NEXTCLOUD_LOCATION}"/nextcloud-23.0.0.zip -d "${NEXTCLOUD_LOCATION}"/
rm "${NEXTCLOUD_LOCATION}"/nextcloud-23.0.0.zip
chown -R www-data:www-data "${NEXTCLOUD_LOCATION}"/nextcloud
chmod -R 700 "${NEXTCLOUD_LOCATION}"/nextcloud
mkdir -p "${NEXTCLOUDDATA_LOCATION}"
chown -R www-data:www-data "${NEXTCLOUDDATA_LOCATION}"
chmod -R 700 "${NEXTCLOUDDATA_LOCATION}"
}

nextcloud_install() {
echo 'Installing Nextcloud...'
sudo -u www-data php "${NEXTCLOUD_LOCATION}"/nextcloud/occ maintenance:install -n --database \
"mysql" --database-name "nextcloud"  --database-user "${NEXTCLOUDUSER}" --database-pass \
"${NEXTCLOUDPASSWD}" --admin-user "${NEXTCLOUDUSER}" --admin-pass "${NEXTCLOUDPASSWD}" \
 --data-dir "${NEXTCLOUDDATA_LOCATION}"
}

apache_configuration() {
mkdir /etc/apache2/ssl
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 \
  -subj "/C=FR/ST=Ile-de-France/L=Paris/O=${SERVERNAME}/CN=${SERVERNAME}" \
  -keyout /etc/apache2/ssl/nextcloud.key  -out /etc/apache2/ssl/nextcloud.cert
cat << APACHE > "${NEXTCLOUD_APACHE}"
alias '/${SERVERNAME}' ${NEXTCLOUD_LOCATION}/nextcloud
<VirtualHost *:443>
  ServerName ${SERVERNAME}
  DocumentRoot ${NEXTCLOUD_LOCATION}/nextcloud
  ErrorLog ${APACHE_LOG_DIR}/nextcloud_error.log
  CustomLog ${APACHE_LOG_DIR}/nextcloud_access.log combined
    <Directory ${NEXTCLOUD_LOCATION}/nextcloud>
      Require all granted
      AllowOverride All
      Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav on
    </IfModule>
    </Directory>
  SSLEngine on
  SSLCertificateFile /etc/apache2/ssl/nextcloud.cert
  SSLCertificateKeyFile /etc/apache2/ssl/nextcloud.key
</VirtualHost>
APACHE
a2enmod ssl
a2ensite nextcloud
APACHEINI='/etc/php/7.4/apache2/php.ini'
  sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 16G/' $APACHEINI
  sed -i 's/post_max_size = 8M/post_max_size = 16G/'             $APACHEINI
  sed -i 's/output_buffering = 4096/output_buffering = 0/'       $APACHEINI
  sed -i 's/memory_limit = 128M/memory_limit = 1024M/'           $APACHEINI
  sed -i 's~;upload_tmp_dir =~upload_tmp_dir = /tmp/nextcloud~'  $APACHEINI
# lighttpd use port 80
if [[ -n $(systemctl status lighttpd | grep running) ]]; then
  systemctl disable --now lighttpd
fi
}

redis_install() {
sed -i '$d' "${NEXTCLOUD_LOCATION}"/nextcloud/config/config.php	# delete ); at the end 
cat << EOF >> "${NEXTCLOUD_LOCATION}"/nextcloud/config/config.php
  'filelocking.enabled' => 'true',
  'memcache.locking' => '\OC\Memcache\Redis',
  'memcache.local' => '\OC\Memcache\Redis',
  'memcache.distributed' => '\OC\Memcache\Redis',
  'redis' => 
  array (
    'host' => 'localhost',
    'port' => 6379,
    'dbindex' => 0,
    'timeout' => 0,
  ),
);
EOF
phpenmod redis
systemctl restart redis-server
systemctl enable redis-server
systemctl restart apache2
}

main(){
questions
dependencies_install  
nextcloud_download
mariadb_configuration
nextcloud_install
apache_configuration
  systemctl restart apache2
  systemctl enable apache2
redis_install
}

NEXTCLOUD_APACHE=/etc/apache2/sites-available/nextcloud.conf
NEXTCLOUD_LOCATION='/var/www'
NEXTCLOUDDATA_LOCATION='/srv/nextcloud/data'

# EXEC
if [[ $1 == '--install' || $1 == '-i' ]]; then
  main
  echo 'Nextcloud install done !'
  echo "Nextcloud website location: ${NEXTCLOUD_LOCATION} with permissions www-data:700"
  echo "Nextcloud data location: ${NEXTCLOUDDATA_LOCATION} with permissions www-data:700" 
  echo "Your nextcloud is accessible via http://localhost/${SERVERNAME}"
  exit 0
fi
if [[ $1 == '--redis-only' || $1 == '-i' ]]; then
  redis_install
  echo 'Redis install done !'
  exit 0
fi
if [[ $1 == '-h' || -z $1 ]]; then
  echo 'Usage: ./nextcloud.sh [OPTION]'
  echo '  -i, --install          install Nextcloud (download nextcloud, configure mariadb, apache2 and install redis)'
  echo '  -r, --redis-only       install redis only'
fi
