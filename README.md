# edricus-nextcloud

Script to deploy Nextcloud with Regis without Docker 

## Getting started

    Usage: ./nextcloud.sh [OPTION]
    -i, --install          install Nextcloud (download nextcloud, configure mariadb, apache2 and install redis)
    -r, --redis-only       install redis only

To Do

- [ ] Whiptail questions 
- [X] SSL without user interaction
- [ ] Certbot integration
